class EndpointsResolverController < ApplicationController
  before_action :set_endpoint
  before_action :prepare_headers

  rescue_from ActiveRecord::RecordNotFound, with: :endpoint_not_found
  rescue_from JSON::ParserError, with: :endpoint_body_invalid

  def resolve
    render json: JSON.parse(@endpoint.response_body)
  end

  private 

  def endpoint_not_found
    render json: {
      errors: [
        {
          code: :not_found,
          details: "Requested page `#{path_params}` does not exist"
        }
      ]
    }, status: :not_found
  end

  def endpoint_body_invalid
    render json: {
      errors: [
        {
          code: :unprocessable_entity,
          details: "Response body of `#{path_params}` is not valid JSON"
        }
      ]
    }, status: :unprocessable_entity
  end

  def set_endpoint 
    @endpoint = Endpoint.find_by!(path: path_params, verb: request.method)
  end

  def path_params
    return params[:path] if params[:path].starts_with?("/")
    "/" + params[:path]
  end

  def prepare_headers
    @endpoint.response_header.each do |k, v|
      response.set_header(k, v)
    end
  end
end
