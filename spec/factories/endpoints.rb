FactoryBot.define do
  factory :endpoint do
    verb { "GET" }
    sequence(:path) { |n| "/foo/bar/baz#{n}" }
    response_code { 200 }
    response_header { { "X-Clacks-Overhead" => "GNU Terry Pratchett" } }
    response_body { %({ "message": "Hello, world" }) }
  end

  factory :invalid_endpoint, class: Endpoint do 
    verb { nil }
    path { nil }
    response_code { "not-a-num" }
    response_header { nil }
    response_body { nil }
  end
end
