class EndpointsController < ApplicationController
  before_action :set_endpoint, only: [:update, :destroy]

  # GET /endpoints
  def index
    render json: Endpoint.all.serialized_json
  end

  # POST /endpoints
  def create
    @endpoint = Endpoint.build_from_jsonapi(endpoint_params)

    if @endpoint.save
      render json: @endpoint.serialized_json, status: :created, 
        location: url_for(controller: "endpoints_resolver", 
                          action: "resolve", path: @endpoint.path, 
                          only_path: false)
    else
      render jsonapi_errors: @endpoint.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /endpoints/1
  def update
    if @endpoint.assign_from_jsonapi(endpoint_params).save
      render json: @endpoint.serialized_json
    else
      render jsonapi_errors: @endpoint.errors, status: :unprocessable_entity
    end
  end

  # DELETE /endpoints/1
  def destroy
    @endpoint.destroy
  end

  private
  def set_endpoint
    @endpoint = Endpoint.find(params[:id])
  end

  def endpoint_params
    params.require(:data).permit(:type, attributes: [ :verb, :path, response: [ :code, :body, headers: {} ] ] )
  end
end
