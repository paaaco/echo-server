require "rails_helper"

RSpec.describe EndpointsResolverController, type: :routing do
  describe "routing" do 
    it "routes everything to endpoints_resolver#resolve" do
      ["OPTIONS", "GET", "HEAD", "PUT", "POST", "DELETE", "PATCH"].each do |http_method|
        expect(
          http_method.downcase.to_sym => "/foo/bar/baz"
        ).to route_to("endpoints_resolver#resolve", path: "foo/bar/baz")
      end
    end

    it "doesn't route /endpoints routes" do 
      expect(get: "/endpoints").to_not route_to("endpoints_resolver#resolve", path: "endpoints")
    end
  end
end
