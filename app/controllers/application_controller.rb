class ApplicationController < ActionController::API
  include JSONAPI::Errors

  before_action :remove_headers
  after_action :set_content_length

  private 

  def remove_headers
    ActionDispatch::Response::default_charset = nil
    response.header.each do |k, _|
      response.delete_header(k)
    end
  end 

  def set_content_length
    response.headers["Content-Length"] = response.body.length
  end
end
