Rails.application.routes.draw do
  resources :endpoints, except: [:show]
  match '/*path', to: "endpoints_resolver#resolve", via: :all
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
