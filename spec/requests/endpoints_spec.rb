require 'rails_helper'

RSpec.describe "/endpoints", type: :request do
  let(:endpoint){ build :endpoint }
  let(:invalid_endpoint){ build :invalid_endpoint }

  let(:valid_attributes) {
    endpoint.serialized_json
  }

  let(:invalid_attributes) {
    invalid_endpoint.serialized_json
  }

  let(:valid_headers) {
    {}
  }

  describe "GET /index" do
    let!(:endpoints) { create_list(:endpoint, 5) }

    before :each do 
      get endpoints_url, headers: valid_headers, as: :json
    end

    it "renders a successful response" do
      expect(response).to be_successful
    end

    it "returns all created endpoints in the system" do
      json_response = JSON.parse(response.body)
      expect(json_response["data"].size).to eq(Endpoint.count)
    end
  end

  describe "POST /create" do
    context "with valid parameters" do
      subject do
        post endpoints_url, params: valid_attributes, headers: valid_headers, as: :json
      end

      it "creates a new Endpoint" do
        expect { subject }.to change(Endpoint, :count).by(1)
      end

      it "renders a JSONAPI response with the new endpoint" do
        subject
        expect(response).to have_http_status(:created)
        expect(response.content_type).to match(a_string_including("application/vnd.api+json"))
      end

      it "should render a response with a Location header pointing to the created endpoint" do
        subject
        expect(response.headers).to have_key("Location")
        expect(response.headers["Location"]).to match("/foo/bar/baz")
      end

      context "when creating an already existing endpoint" do 
        let!(:existing_endpoint){ create(:endpoint, verb: "GET", path: "/hello") }

        subject do 
          post endpoints_url, params: existing_endpoint.serialized_json, headers: valid_headers, as: :json
        end

        it "does not create a new endpoint" do
          expect { subject }.to change(Endpoint, :count).by(0)
        end
      end
    end

    context "with invalid parameters" do
      it "does not create a new Endpoint" do
        expect {
          post endpoints_url,
               params: invalid_attributes, as: :json
        }.to change(Endpoint, :count).by(0)
      end

      it "renders a JSONAPI response with errors for the new endpoint" do
        post endpoints_url,
             params: invalid_attributes, headers: valid_headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to match(a_string_including("application/vnd.api+json"))
      end
    end

    context "with invalid JSONAPI format parameters" do
      let(:invalid_format) do
        {
          "not_a_valid_jsonapi": true
        }
      end
      
      subject do
        post endpoints_url, params: invalid_format, headers: valid_headers, as: :json
      end

      it "should not create a new Endpoints" do
        expect { subject }.to change(Endpoint, :count).by(0)
      end

      it "should return :unprocessable_entity" do 
        subject
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe "PATCH /update" do
    let(:endpoint) { create :endpoint }

    context "with valid parameters" do
      let(:new_attributes) {
        { 
          data: {
            id: endpoint.id,
            type: "endpoints",
            attributes: {
              path: "/new/path/whodis"
            }
          } 
        }
      }

      let(:invalid_attributes) {
        { 
          data: {
            id: endpoint.id,
            type: "endpoints",
            attributes: {
              response: {
                code: 0
              }
            }
          } 
        }
      }

      it "updates the requested endpoint" do
        patch endpoint_url(endpoint),
              params: new_attributes, headers: valid_headers, as: :json
        endpoint.reload
        expect(endpoint.path).to eq("/new/path/whodis")
      end

      it "renders a JSON response with the endpoint" do
        patch endpoint_url(endpoint),
              params: new_attributes, headers: valid_headers, as: :json
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to match(a_string_including("application/vnd.api+json"))
      end
    end

    context "with invalid parameters" do
      it "renders a JSON response with errors for the endpoint" do
        patch endpoint_url(endpoint),
              params: invalid_attributes, headers: valid_headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to match(a_string_including("application/vnd.api+json"))
      end
    end

    context "with invalid JSONAPI format parameters" do
      let(:invalid_format) do
        {
          "not_a_valid_jsonapi": true
        }
      end
      
      subject do
        patch endpoint_url(endpoint),
              params: invalid_format, headers: valid_headers, as: :json
      end

      it "should not change the given endpoint" do
        expect { subject }.to_not change(endpoint, :updated_at)
      end

      it "should return :unprocessable_entity" do 
        subject
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe "DELETE /destroy" do
    let!(:endpoint) { create :endpoint }

    subject do 
      delete endpoint_url(endpoint), headers: valid_headers, as: :json
    end

    it "destroys the requested endpoint" do
      expect {
        subject
      }.to change(Endpoint, :count).by(-1)
    end

    it "renders a 204 No Content response" do 
      subject
      expect(response).to have_http_status(:no_content)
    end
  end
end
