require 'rails_helper'

RSpec.describe "EndpointsResolvers", type: :request do
  let(:endpoint) { create :endpoint }

  describe "/resolve" do
    it "returns http success" do
      get "/#{endpoint.path}"
      expect(response).to have_http_status(:success)
    end

    context "when given a User-generated Endpoint" do
      let(:endpoint) { create(:endpoint) } 

      before :each do
        send(endpoint.verb.downcase.to_sym, endpoint.path)
      end

      it "should return the specified HTTP code" do 
        expect(response.code.to_i).to eq(endpoint.response_code)
      end

      it "should return the specified HTTP header" do
        endpoint.response_header.each do |key, value|
          expect(response.headers).to have_key(key)
        end 
      end

      it "should return the specified response body" do
        expect(JSON.parse response.body).to eq(JSON.parse endpoint.response_body)
      end
    end

    context "when given an endpoint that does not exist" do 
      before :each do 
        get '/non/existent/endpoint'
      end

      it "should return code 404" do
        expect(response.code).to eq("404") 
      end

      it "should return an error object in body" do
        json_response = JSON.parse(response.body)

        expect(json_response).to have_key("errors")
        expect(json_response["errors"]).to be_a(Array)
        expect(json_response["errors"][0]["details"]).to match("Requested page `/non/existent/endpoint` does not exist")
      end
    end

    context "when given a path that exists, but with a different HTTP method" do 
      let(:endpoint) { create(:endpoint, verb: "GET") }

      before :each do
        post endpoint.path 
      end

      it "should return code 404" do
        expect(response.code).to eq("404")
      end
    end
  end
end
