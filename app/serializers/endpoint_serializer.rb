class EndpointSerializer
  include JSONAPI::Serializer
  set_type :endpoints
  set_id :id
  attributes :verb, :path

  attribute :response do |obj|
    {
      code: obj.response_code,
      headers: obj.response_header,
      body: obj.response_body
    }
  end
end
