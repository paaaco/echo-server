class Endpoint < ApplicationRecord
  validates :verb, :path, presence: true
  validates :verb, inclusion: ["GET", "POST", "PUT", "PATCH", "OPTIONS", "DELETE", "HEAD"]
  validates :path, uniqueness: { scope: [:verb] }
  validates :response_code, numericality: { greater_than: 99, less_than: 600 }

  scope :serialized_json, -> { EndpointSerializer.new(self).serializable_hash }

  serialize :response_header, Hash

  before_validation :prepend_forward_slash, if: Proc.new{ path }

  def serialized_json
    EndpointSerializer.new(self).serializable_hash
  end

  def assign_from_jsonapi(jsonapi)
    assign_attributes(Endpoint.deserialize_jsonapi(jsonapi))
    self
  end

  def self.build_from_jsonapi(jsonapi)
    Endpoint.new(deserialize_jsonapi(jsonapi))
  end

  private

  def prepend_forward_slash
    return if path.starts_with?("/")
    self.path = "/#{path}"
  end

  def self.deserialize_jsonapi(jsonapi)
    flat_attributes = {}
    [
      [:id, [:id]],
      [:verb, [:attributes, :verb]],
      [:path, [:attributes, :path]],
      [:response_code, [:attributes, :response, :code]],
      [:response_header, [:attributes, :response, :headers]],
      [:response_body, [:attributes, :response, :body]]
    ].each do |key, value|
      next if jsonapi.dig(*value).blank?
      if key == :response_header
        flat_attributes[key] = jsonapi.dig(*value).to_hash
      else
        flat_attributes[key] = jsonapi.dig(*value)
      end
    end

    flat_attributes
  end
end
