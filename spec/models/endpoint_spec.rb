require 'rails_helper'

RSpec.describe Endpoint, type: :model do
  let(:endpoint) { create(:endpoint) }

  describe "Validations" do
    context "verb" do
      it "must exist" do
        endpoint.verb = nil
        expect(endpoint).to_not be_valid
      end

      it "must be a valid HTTP verb" do
        endpoint.verb = "RUN"
        expect(endpoint).to_not be_valid
        
        endpoint.verb = "SLEEP"
        expect(endpoint).to_not be_valid

        ["GET", "POST", "PATCH"].each do |http_method|
          endpoint.verb = http_method
          expect(endpoint).to be_valid
        end
      end
    end

    context "path" do
      it "must exist" do
        endpoint.path = nil
        expect(endpoint).to_not be_valid
      end
    end

    context "response code" do
      it "must be between 100 and 599" do
        endpoint.response_code = 99
        expect(endpoint).to_not be_valid

        endpoint.response_code = 600
        expect(endpoint).to_not be_valid
      end
    end

    context "uniqueness" do 
      let!(:existing_endpoint) { create(:endpoint, verb: "GET", path: "/hello/yes") }
      
      it "must be unique based on path and verb" do
        new_endpoint = build(:endpoint, verb: "GET", path: "/hello/yes")
        expect(new_endpoint).to_not be_valid
      end
    end
  end

  describe "Before save" do
    context "path" do
      it "should prepend forward slash `/` if it doesn't have" do
        endpoint.path = "hello"
        endpoint.save
        endpoint.reload
        expect(endpoint.path).to eq("/hello")
      end

      it "should do nothing if forward slash already present" do
        endpoint.path = "/hello"
        endpoint.save
        endpoint.reload
        expect(endpoint.path).to eq("/hello")
      end
    end
  end

  describe "Model Methods" do 
    describe "#serialized_json" do
      subject do 
        endpoint.serialized_json[:data]
      end

      it "should return a Hash object" do 
        expect(subject).to be_a(Hash)
      end

      it "should have a type key" do 
        expect(subject).to have_key(:type)
        expect(subject[:type]).to eq(:endpoints)
      end 

      it "should have a id key" do 
        expect(subject).to have_key(:id)
        expect(subject[:id]).to eq(endpoint.id.to_s)
      end

      it "should have a attributes key" do 
        expect(subject).to have_key(:attributes)
      end

      context "the attributes key" do
        it "should have a Hash value" do 
          expect(subject[:attributes]).to be_a(Hash)
        end

        it "should have a verb key" do 
          expect(subject[:attributes]).to have_key(:verb)
          expect(subject[:attributes][:verb]).to eq(endpoint.verb)
        end

        it "should have a path key" do 
          expect(subject[:attributes]).to have_key(:path)
          expect(subject[:attributes][:path]).to eq(endpoint.path)
        end

        it "should have a response key" do 
          expect(subject[:attributes]).to have_key(:response)
        end

        context "the response key" do
          it "should have a Hash value" do 
            expect(subject[:attributes][:response]).to be_a(Hash)
          end

          it "should have a code key" do 
            expect(subject[:attributes][:response]).to have_key(:code)
            expect(subject[:attributes][:response][:code]).to eq(endpoint.response_code)
          end

          it "should have a headers key" do 
            expect(subject[:attributes][:response]).to have_key(:headers)
            expect(subject[:attributes][:response][:headers]).to eq(endpoint.response_header)

          end

          it "should have a body key" do 
            expect(subject[:attributes][:response]).to have_key(:body)
            expect(subject[:attributes][:response][:body]).to eq(endpoint.response_body)
          end
        end
      end
    end
  end

  describe "Class Methods/Scopes" do 
    describe "#serialized_json" do
      let!(:endpoints) { create_list(:endpoint, 5) } 

      subject do 
        Endpoint.all.serialized_json
      end

      it "should return a JSONAPIv1 object hash" do 
        expect(subject).to be_a(Hash)
      end

      it "should return a data object containing all Endpoints" do 
        expect(subject[:data]).to be_a(Array)
        expect(subject[:data].size).to eq(5)
      end
    end
  end
end
