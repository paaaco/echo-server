class HeaderSuppressor
  def initialize(app, options = {})
    @app, @options = app, options
  end

  def call(env)
    r = @app.call(env)

    #[status, headers, response] = r

    r[1].delete "X-Runtime"
    r[1].delete "X-Powered-By"
    r[1].delete "Server"
    r[1].delete "Transfer-Encoding"
    r[1].delete "ETag"
    r[1].delete "Cache-Control"
    r[1].delete "X-Request-Id"
    r
  end
end