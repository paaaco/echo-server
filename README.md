# README

* The main purpose of Echo is to serve ephemeral/mock endpoints created with parameters specified by clients.

## Local Setup

* `$ git clone` this repository, `cd` into it, and run `$ bundle install`.
* run migrations and seeds with `rake db:create db:migrate db:seed`
  * Running seeds is optional, it contains a couple of Endpoint records.
  * Database is just a standard sqlite database.
* run server with `$ rails s`.

## Automated Testing

* This application's testing suite is RSpec. You can run rspec tests with `$ rspec`

## Design Choices

* I used the `jsonapi-serializer` gem initially to handle serialization of Endpoint models in the JSONAPI format. Later on, I realized I needed to handle deserialization/validation well, so I went with the `jsonapi.rb` gem. 
* I initially went with Rails (in API-only mode) for this project to handle stuff like HTTP requests and database management. However as I went further along the project, I realized it might have been better to make a smaller application with something like Sinatra and just load in additional dependencies I might need.
