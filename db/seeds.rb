# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Endpoint.create(
  path: "/foo/bar/baz",
  verb: "GET",
  response_code: 200,
  response_header: { "X-Clacks-Overhead" => "GNU Terry Pratchett" },
  response_body: %({ "hi": "hello" })
)

Endpoint.create(
  path: "/foo/bar/baz",
  verb: "POST",
  response_code: 200,
  response_header: {},
  response_body: %({ "hi": "bye" })
)